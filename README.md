# Mekamon Smart Module CAD files

----

CAD files for the 3D-printed attachment plate supplied with the MSM, for attaching a Raspberry Pi Zero and camera to the MekaMon Berserker.

These are made available here for users to modify freely and print.

Other versions for larger Pi boards may be added soon.

The models were created in Solidworks and exported as .STL files. They need 'slicing' in Repetier-Host, Cura or similar to prepare them for your printer.

If you have any issues please contact wesley@reachrobotics.com